#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidget>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void addTreeRoot(QString name, QString description);
    void addTreeChild(QTreeWidgetItem *parent,
                      QString name, QString description);
    void loadStylesheet(const QString& stylesheetAlias);
    Ui::MainWindow *ui;
    QAction *action_;
    QMenu *menu_;
    QToolBar *toolBar_;

    QMenuBar *menuBar_;
};

#endif // MAINWINDOW_H
