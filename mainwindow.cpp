#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    loadStylesheet(":/qss/StyleSheet");
    ui->treeWidget->setColumnCount(2);
       // Add root nodes
       addTreeRoot("A", "Root_first");
       addTreeRoot("B", "Root_second");
       addTreeRoot("C", "Root_third");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadStylesheet(const QString& stylesheetAlias)
{
    QFile stylesheetFile(stylesheetAlias);
    QString stylesheetContent;
    if (stylesheetFile.open(QFile::ReadOnly)) {
        QTextStream styleIn(&stylesheetFile);
        stylesheetContent = styleIn.readAll();
        stylesheetFile.close();
        setStyleSheet(stylesheetContent);
    }

    action_ = new QAction(QIcon(":/media/DropDownBlack"), tr("Action"), this);
    action_->setToolTip("Action");
    menu_ = ui->menuBar->addMenu("Menu");
    menu_->addAction(action_);

    ui->menuBar->addAction(action_);
}


void MainWindow::addTreeRoot(QString name, QString description)
{
    QTreeWidgetItem *treeItem = new QTreeWidgetItem(ui->treeWidget);

    treeItem->setText(0, name);
    treeItem->setText(1, description);
    addTreeChild(treeItem, name + "A", "Child_first");
    addTreeChild(treeItem, name + "B", "Child_second");
}

void MainWindow::addTreeChild(QTreeWidgetItem *parent,
                  QString name, QString description)
{
    QTreeWidgetItem *treeItem = new QTreeWidgetItem();

    treeItem->setText(0, name);
    treeItem->setText(1, description);

    parent->addChild(treeItem);
}
